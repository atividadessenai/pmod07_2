package com.senai.guilherme.pmod07_2;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.IdRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabReselectListener;
import com.roughike.bottombar.OnTabSelectListener;

public class TelaPrincipal extends AppCompatActivity {

    protected void showBottomBarOptions(int tabId) {

        final TextView tvStatus = (TextView)findViewById(R.id.tvStatus);

        if(tabId == R.id.tab_buscar_contatos) {
            new AlertDialog.Builder(TelaPrincipal.this)
                    .setTitle(R.string.txBuscar)
                    .setMessage("Buscar contato?")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .show();
        }
        if(tabId == R.id.tab_listar_contatos) {
            tvStatus.setText(R.string.txLista);
        }
        if (tabId == R.id.tab_adicionar_contato) {
            String[] types = {"Agenda", "Detalhes do contato"};
            new AlertDialog.Builder(TelaPrincipal.this)
                    .setTitle(R.string.txBuscar)
                    .setItems(types, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int idItem) {
                            if (idItem == 0) {
                                tvStatus.setText("Agenda");
                            } else if (idItem == 1) {
                                tvStatus.setText("Detalhes do contato");
                            }
                        }
                    })
                    .show();
        }
        if(tabId == R.id.tab_fechar_aplicativo) {
            new AlertDialog.Builder(TelaPrincipal.this)
                    .setTitle("Fechar atividade")
                    .setMessage("Deseja encerrar?")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) { }
                    })
                    .setIcon(R.drawable.ic_warning)
                    .show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_principal);

        BottomBar bottomBar = (BottomBar)findViewById(R.id.bottomBar);
        bottomBar.setOnTabReselectListener(new OnTabReselectListener() {
            @Override
            public void onTabReSelected(@IdRes int tabId) {
                showBottomBarOptions(tabId);
            }
        });

        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                showBottomBarOptions(tabId);
            }
        });
    }
}
